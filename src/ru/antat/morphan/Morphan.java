package ru.antat.morphan;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;

import net.sf.hfst.NoTokenizationException;
import net.sf.hfst.Transducer;
import net.sf.hfst.TransducerAlphabet;
import net.sf.hfst.TransducerHeader;
import net.sf.hfst.UnweightedTransducer;
import net.sf.hfst.WeightedTransducer;

public class Morphan {
	private Transducer transducer = null;
	private Hashtable<String,String> allomorphs = null;
	
	public Morphan(String hfstol_file){
		File file = new File(hfstol_file);
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(file);
			TransducerHeader th = new TransducerHeader(fis);
            DataInputStream charstream = new DataInputStream(fis);
            TransducerAlphabet ta = new TransducerAlphabet(charstream, th.getSymbolCount());
            if (th.isWeighted()) {
                transducer = new WeightedTransducer(fis, th, ta);
	        } else {
	            transducer = new UnweightedTransducer(fis, th, ta);
	        }
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (fis != null)
					fis.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}
	
	public Morphan(String hfstolFile,String allomorphsFile){
		File file = new File(hfstolFile);
		FileInputStream fis = null;
		try {
			//загрузка трансдьюсера
			fis = new FileInputStream(file);
			TransducerHeader th = new TransducerHeader(fis);
            DataInputStream charstream = new DataInputStream(fis);
            TransducerAlphabet ta = new TransducerAlphabet(charstream, th.getSymbolCount());
            if (th.isWeighted()) {
                transducer = new WeightedTransducer(fis, th, ta);
	        } else {
	            transducer = new UnweightedTransducer(fis, th, ta);
	        }
            //загрузка словаря алломорфов
            byte[] bytes = Files.readAllBytes(Paths.get(allomorphsFile));
			String s = new String(bytes, "UTF-8");
			
			allomorphs = new Hashtable<String,String>();
			String[] res = s.split("\n");
			for (String line : res){
				String[] el = line.split("\t");
				allomorphs.put(el[0]+"("+el[1]+")", el[2].substring(0, el[2].length()-1));
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (fis != null)
					fis.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}
	
	public String analyze(String word){
		String res = "";
		try {
			Collection<String> analyses = transducer.analyze(word);
			for (String analysis : analyses) {
				String[] parts = analysis.split("\t");
	            res += parts[0]+';';
	        }
		} catch (NoTokenizationException e) {
			res = "NR";
		}
		if (res.length() == 0)  res = "NR";
		return res;
	}
	

	public String analyze(String word, String mode){
		String res = "";
		Set<String> myres = new HashSet<String>();
		try {
			Collection<String> analyses = transducer.analyze(word);
			for (String analysis : analyses) {
				String[] parts = analysis.split("\t");
				String[] morph = parts[0].split("\\+");
				List<String> all = get_next_allomorph(word.toLowerCase(), morph, morph.length-1,mode);
				if (all != null) myres.addAll(all);				
	        }
			for (String r : myres) res +=r+";";
		} catch (NoTokenizationException e) {
			res = "NR";
		}
		if (res.length() == 0)  res = "NR";
		return res;
	}
	
	public List<String> get_next_allomorph(String word,String[] morph,Integer i,String mode){
		List<String> res = new ArrayList<String>();
		if (i<2){
			if (word.equals(morph[0])){
				if (mode.equals("1")) res.add(morph[0]);
				else if (mode.equals("2")) res.add(morph[1]+"("+morph[0]+")");
				else res.add(morph[0]+"+"+morph[1]);
				return res;
			}else return null;
		}
		String a = allomorphs.get(morph[i]);
		if (a == null){
			return null;
		}
		String[] ams = a.split(";");
		List<String> prev;
		int j = 0;
		String[] mk;
		for (String am : ams){
			j = word.length()-am.length();
			if (j > 0){
				if (am.equals(word.substring(j, word.length()))){
					prev = get_next_allomorph(word.substring(0,j),morph,i-1,mode);
					if (prev != null){
						for (String pr : prev){
							if (mode.equals("1")){
								if (am.equals("")) res.add(pr);
								else res.add(pr+"+"+am);
							}else{
								mk = morph[i].split("\\(");
								res.add(pr+"+"+mk[0]+"("+am+")");
							}
						}
					}
				}
			}
		}
		
		if (res.isEmpty()) return null;
		return res;
	}
}
