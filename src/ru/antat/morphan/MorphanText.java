package ru.antat.morphan;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class MorphanText {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		long ds = java.lang.System.currentTimeMillis();
		Morphan m = new Morphan("lib/tatar_last.hfstol", "lib/allomorphs.csv");
		String fileName = "texts/test.txt";
		String outFileName = "";
		String mode = "2";
		if (args.length>1) fileName = args[1];
		if (args.length>0) mode = args[0];
        if (fileName.substring(fileName.length()-1) == "/") fileName = fileName.substring(0, fileName.length()-2);
		File fl = new File(fileName);
		String res;
		if (fl.exists()){
			if (fl.isFile()){
				if (args.length>2) outFileName = args[2];
				else{
					String[] f = fileName.split("/");
					f[f.length-1] = "analyzed_" + f[f.length-1];
					for (String fs : f){
						outFileName = outFileName + "/" + fs;
					}
					outFileName = outFileName.substring(1);
				}
				res = analyzeText(fileName,m,mode);
				if (res != "-1") writeToFile(outFileName,res);
			}else{
				String outFolder;
				if (args.length>1) {
					outFolder = args[1];
			        if (outFolder.substring(outFolder.length()-1) != "/") outFolder = outFolder + "/";
					File outf = new File(outFolder);
					if (!outf.exists()) outf.mkdirs();
					if (!outf.isDirectory()){
						outFolder = fileName + "/analyzed/";
		            	new File(outFolder).mkdir();
					}
				} else {
	            	outFolder = fileName + "/analyzed/";
	            	new File(outFolder).mkdir();
	            }
				for (final File fileEntry : fl.listFiles()) {
			        if (fileEntry.isFile()) {
			            System.out.println("Анализирую файл: "+fileEntry);
			            fileName = fileEntry.getAbsolutePath();
			            res = analyzeText(fileName,m,mode); 
			            outFileName = outFolder + "/" + fileEntry.getName();
						if (res != "-1") writeToFile(outFileName,res);
			        }
			    }
			}
			long col = (java.lang.System.currentTimeMillis()-ds)/1000;
			System.out.print(col);
		}
		else
		{
			System.out.println("Ошибка: указанный файл/папка не существует");
		}
	}
	
	public static String analyzeText(String fileName,Morphan m,String mode){
		String res = "";
		try {
//            List<String> lines = Files.readAllLines(Paths.get(fileName),StandardCharsets.UTF_8);
			byte[] bytes = Files.readAllBytes(Paths.get(fileName));
			String line = new String(bytes, "UTF-8");
            line = line.replace(".", " . ").replace("!", " ! ").replace("?", " ? ").replace(",", " , ");
            String[] words = line.split("[ \t\n]+");
            for (String word : words){
            	switch (word){
            	case ".": res = res + word + "\nType1\n"; break;
            	case "!": res = res + word + "\nType1\n"; break;
            	case "?": res = res + word + "\nType1\n"; break;
            	case ",": res = res + word + "\nType2\n"; break;
            	case "\r": break;
            	default: 
            		if (mode.equals("0")) res = res + word + "\n" + m.analyze(word) + "\n";
            		else res = res + word + "\n" + m.analyze(word,mode) + "\n";
            	}
            }
    		return res;
        } catch (IOException e) {
            e.printStackTrace();
        	System.out.println("Ошибка считывания файла!");
        	return "-1";
        }
	}
	
	public static void writeToFile(String fileName,String result){
		PrintWriter writer;
		try {
			writer = new PrintWriter(fileName, "UTF-8");
			writer.print(result);
			writer.close();
			System.out.println("Результат записан в файл: "+fileName);
		} catch (FileNotFoundException e){
			System.out.println("Ошибка записи файла: файл не найден, либо не может быть записан");			
		} catch (UnsupportedEncodingException e) {
			System.out.println("Ошибка записи файла: Неправильная кодировка");
		}
	}
}
