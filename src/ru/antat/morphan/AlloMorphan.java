package ru.antat.morphan;

public class AlloMorphan {

	public static void main(String[] args) {
		Morphan m = new Morphan("lib/tatar_last.hfstol", "lib/allomorphs.csv");
		if (args.length>1) System.out.println(m.analyze(args[0],args[1]));
		else if (args.length>0) System.out.println(m.analyze(args[0]));
		else System.out.println("NR");
//		System.out.println(m.analyze("бар","1"));
//		System.out.println(m.analyze("урманныкыларга","1"));
//		System.out.println(m.analyze("урманныкыларга","2"));
//		System.out.println(m.analyze("урманныкыларга","3"));
	}
}